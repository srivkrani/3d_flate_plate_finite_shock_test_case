!----------------------------------------------------------------------------------------------------------------------------------!
! module which defines input parameters and calculates some other parameters which are used by other subroutines.
!----------------------------------------------------------------------------------------------------------------------------------!
module params

#include "setup.h"

   implicit none

!----- Constants/ variables defined in input file ---------------------------------------------------------------------------------!
!--- flow properties
   real*8, public                          :: gam, &   !gamma, ratio of specific heats
                                            ma,  &   !Mach number
					    pr,  &   !Prandtl number
					    re,  &   !Reynolds number
					    t_fs     !free stream temperature (dimensional in Kelvin)

!--- grid information
   integer, parameter, public            :: nx=3700, ny=600, nz=1 !number of points in the x, y and z/phi direction
   integer, public                       :: nzp,     &          !number of Fourier modes in z/phi direction 
                                            nzpread             !number of Fourier modes in z/phi direction in input file 

!--- domain information
   real, public                          :: x_0, &   !start of domain in x-direction
                                            x_L, &   !end of domain in x-direction
					    y_0, &   !start of domain in y-direction (typically 0 since strating at the wall)
					    y_H, &   !end of domain in y-direction
					    z_l, &   !start of domain in z/phi direction (typicall 0)
					    z_r      !end of domain in z/phi direction           
   
!--- grid stretching (Zhong)
   real, public		                 :: alpha_y  !stretching factor for Zhong's stretching in y
   real, public	           	         :: alpha_x  !stretching factor for Zhong's stretching in x

!--- grid stretching with 3rd order polynomial in wall-normal direction (y)       
   real,public                           :: y_2      !fraction of whole domain height, below which an equidistand grid is used
   integer,public                        :: ny_2     !grid point up to which equidistand grid is employed

!--- initial condition from profcom
   real, public                          :: deta     !delta eta of profcom file in case similarity solution is read in 

!--- time
   integer, public                       :: t_be,    &  !starting time step
                                            t_en,    &  !ending time step
					    t_inter, &  !intermediate output is written out every t_inter time step
					    t_notice    !notice file is written out every t_notice time step

!--- time step
   real, public                          :: dt          !time step
   
!--- varaibales for file names
   character(LEN=127), public            :: RFNAME ,&   !file name for similarity profile from profkom
                                            CFNAME ,&   !file name for continuation file 
                                            WFNAME ,&   !file name for final output file
                                            IFNAME ,&   !file name for intermediate output file
                                            GFNAME ,&   !file name for grid file
				            BFNAME ,&   !file name for base flow file
				            SFNAME      !file name for file used for shock detection
					    

!--- disturbance strip
   integer, public                       :: nf                         !number of different disturbance modes
   real, public                          :: dist_start_x , dist_end_x  !start and end of disturbance strip in x
   real, public                          :: dist_start_z , dist_end_z  !start and end of disturbance strip in z/phi


!--- flux-splitting
   integer, public		         :: supersonic_y ! y-location above which flow is always supersonic
							      ! this will speed up the simulation, since 
							      ! flux-splitting is expensive (only for upwinding in x)
   integer, public		         :: no_upwind_y ! y-location below which no upwinding is use (only for upwinding in y) 

!--- filtering
!--- x-direction
   real,  public                         :: alpha_filter_x                    !variable determines strength of filter (1 no filter,0 strong filter)
   integer,  public                      :: start_xfilter_x ,end_xfilter_x    !start and end of filter in x (points away from either start or end of compuational domsin)
   integer,  public                      :: start_xfilter_y , end_xfilter_y   !start and end of filter in y (points away from either start or end of compuational domsin)


!---y-direction
   real,  public                         :: alpha_filter_y 		      !variable determines strength of filter (1 no filter,0 strong filter)
   integer,  public                      :: start_yfilter_y , end_yfilter_y   !start and end of filter in x (points away from either start or end of compuational domsin)
   integer,  public                      :: start_yfilter_x , end_yfilter_x   !start and end of filter in y (points away from either start or end of compuational domsin)

!---z-direction
   integer, parameter, public            :: global_x_nzp_filter = 1000        !up to which grid point the modes in z/phi-direction should be filtered
   integer, parameter, public            :: nzp_filter = 16                   !mode number above which modes are z/phi modes are set to zero
   
!--- outflow ramp
   integer,  public                      :: start_ramp ,end_ramp              !start and end location of outflow ramp

!--- freestream boundary condition: exponetial decay condition
   real,  public                         :: alpha_TS !wave number of instability wave in x
   real,  public                         :: c_TS     !phase speed of instability wave in x
   real,  public                         :: gamma_TS !spanwise wave number of intabiltiy wave in z 

!--- cone geometry (radius at origin, half angle...)
   real, public                          :: r0                 !radius at virtuel origin of cone        
   real, public                          :: theta              !half angle of cone
   real, public                          :: sintheta, costheta !sin and cos of half angle

!--- roughness variables for implementation with one sided stencils (does not always work)
   integer, public                       :: roughness_start_x  !grid point in x where roughness starts
   integer, public                       :: roughness_end_x    !grid point in x where roughness ends
   integer, public                       :: roughness_end_y    !grid point in y where roughness ends
   integer, public                       :: roughness_start_z  !grid point in z where roughness starts
   integer, public                       :: roughness_end_z    !grid point in z where roughness ends

!--- variables for MPI-parallelization
   integer, public                       :: domains_x,domains_y !number of subdomains in x and y direction

!--- variables for time dependent inflow
   integer, public                       :: t_slp       !number of timesteps to be saved or loaded for time dependent inflow
   integer, public                       :: start_slp   !position where time dependent flow field should be written out
   integer, public                       :: i_slp       !length of grid point interval where time dependent inflow is enforced  


!--- output for intermediate file
   integer, public, save                 :: ny_inter          !number grid point in y starting from the wall to be written out in intermediate output file
   integer, public, save                 :: nx_inter_start    !grid point where the output in x begins for intermediate output file
   integer, public, save                 :: nx_inter_end      !grid point ehere the output in x ends for intermediate output file
   integer, public, save                 :: nzp_write         !number of Fourier modes in z/phi direction to be written out in intermediate output file

!--- Volume forcing
#if (FORCE_INITIAL)||(VOLUME_FORCE ==1)
   real*8, dimension(:,:,:), allocatable, public   :: force_cont      !array for forcing the continuity equation
   real*8, dimension(:,:,:), allocatable, public   :: force_xmom      !array for forcing the x-momentum equation
   real*8, dimension(:,:,:), allocatable, public   :: force_ymom      !array for forcing the y-momentum equation
   real*8, dimension(:,:,:), allocatable, public   :: force_zmom      !array for forcing the z-momentum equation
   real*8, dimension(:,:,:), allocatable, public   :: force_ener      !array for forcing the energy equation
   real*8, public				   :: force_wallpres  !value to force the wall pressure
#endif (FORCE_INITIAL)

!--- Volume forcing
#if (VOLUME_FORCE == 1)
   real*8, dimension(:),allocatable      :: b_vf, c_vf, x0_vf, y0_vf, f_vf, theta_vf, a_vf
   real*8, dimension(:,:,:),allocatable  :: s_vf
   integer                               :: n_vf
   integer, dimension(:),allocatable     :: mode_vf
#endif

!--- moving domain transient reallocation of domain for load balancing
#if (TRANS_REALLOC == 1)
   integer                               :: subx_start, subx_end
   integer                               :: suby_start, suby_end
   integer                               :: subz_start, subz_end
   real*8                                :: epsilon_realloc 
   integer                               :: subx_start_check,subx_start_advance
   integer                               :: subx_end_check,subx_end_advance
   integer                               :: suby_end_check,suby_end_advance
   integer                               :: suby_end_in,subx_end_in
   integer                               :: subz_end_check,subz_end_advance
   integer                               :: subz_end_in
#endif

!--- shock detecting
   real*8                                :: is_shock  !value of smoothness estimator at which the code will say there is a shock, switch to lower order stencil 

!--- miscellaneous variables
   integer, public                               :: t_en_1,mt_1,inter_count
#if defined(FFT)
#else !FFT
   real, dimension(:), allocatable,public,save	   :: dist_amp, dist_freq, dist_phase, modez
#endif !FFT
   real, parameter, public		 :: z_wavelength=0.261799387d0*2.0d0
   real, public                          :: dx, dxx, dy, dyy

#if (GEOMETRY == 3)||(GEOMETRY == 4)
#if (TVD_SCHEME == 1)
   real, parameter,public 		:: gcfac=1.0d0
#else
   real, parameter,public 		:: gcfac=100.0d0
#endif
#endif

#if (MULTI_TS == 1)||(MULTI_TS == 3)
   integer, public                       :: s_multits
   integer, public                       :: r_multits 
   real, public                          :: dt_input
#endif !MULTI_TS

#if (ROUGHNESS == 1)||(POROSITY==1)
!
! x_r_start => starting point of roughness element (physical)
! gamma_r => roughness aspect ratio: gamma_r = delta_x_r/k_r (real variable)
! k_r => roughness height (physical)
!
  real*8:: x_r_start_phys		! physical start point of roughness element(s)
  real*8:: roughness_width		! physical width of roughness element(s)
  real*8:: roughness_depth		! physical depth of roughness elements
  real*8:: roughness_shift		! shift between two roughness elements only necessary for distributed roughness
  integer :: n_roughness		! number of roughness elements

#endif

#if (POROSITY==1)
  real*8:: x_porous_start		! physical start point of porous layer
  real*8:: x_porous_end			! physical end point of porous layer
  integer:: n_cavities			! number of cavities in porous layer
  real*8:: porosity				! porosity
  real*8:: cavity_depth			! cavity depth
#endif

! initial condition for the entire flow field
#if (ALLO == 1)
#if (AOA)
   real, dimension(:,:,:), allocatable, public, save  :: dens_ana, temp_ana, uvel_ana
   real, dimension(:,:,:), allocatable, public, save  :: vvel_ana, pres_ana
   real, dimension(:,:,:), allocatable, public, save  :: duvel_ana, dvvel_ana,toen_ana
#if (SPACE_DIM == 3)
   real, dimension(:,:,:), allocatable, public, save  :: wvel_ana, dwvel_ana
#endif !SPACE_DIM
#else
   real, dimension(:,:), allocatable, public, save    :: dens_ana, temp_ana, uvel_ana
   real, dimension(:,:), allocatable, public, save    :: vvel_ana, pres_ana
   real, dimension(:,:), allocatable, public, save    :: duvel_ana, dvvel_ana,toen_ana
#if (SPACE_DIM == 3)
   real, dimension(:,:), allocatable, public, save    :: wvel_ana, dwvel_ana
#endif !SPACE_DIM
#endif

#else !ALLO
   real, dimension(local_nx,local_ny), public, save  :: dens_ana, temp_ana, uvel_ana
   real, dimension(local_nx,local_ny), public, save  :: vvel_ana, pres_ana
   real, dimension(local_nx,local_ny), public, save  :: duvel_ana, dvvel_ana,toen_ana
#if (SPACE_DIM == 3)
   real, dimension(local_nx,local_ny), public, save  :: wvel_ana, dwvel_ana
#endif !SPACE_DIM
#endif !ALLO
 
! ----- Variables -----------------------------------------------------------------------------------------------------------------!
!--- for definitions see calculation below
   real, public, save :: pi, pi2, i3, n2i3, n4i3, n3i2, dti2, dti3, dt2i3, dti4, dti6
   
   real, public, save :: g1, gg1, gm2, g1m2, gg1m2, g1m2r, g1m2p, g1m2rp

   real, public, save :: im, ire, igm2, ig1m2, igg1m2, ig1m2p, ig1m2rp, ig, ig1, i2g21, i2g, im2

   real, public, save :: g1ig, g1isqrt2g21, n2ig, n2isqrt2g21

contains

!----------------------------------------------------------------------------------------------------------------------------------!
! first subroutine: Calculates some parameters for later.
!----------------------------------------------------------------------------------------------------------------------------------!
   subroutine params_init

!--- claculation of important variables
   pi = 4.0d0 * datan(1.0d0)
   pi2= 2.0d0 * pi

   i3   = 1.0d0/3.0d0
   n2i3 = 2.0d0*i3
   n4i3 = 2.0d0*n2i3
   n3i2 = 3.0d0/2.0d0
  
   dti2 = -dt/2.0d0
   dti3 = -dt/3.0d0
   dt2i3=  dti3*2.0d0
   dti4 = -dt/4.0d0
   dti6 = -dt/6.0d0

   g1     =  gam-1.0d0
   gg1    =  gam*g1 
   gm2    =  gam*ma*ma
   g1m2   =   g1*ma*ma
   gg1m2  =   g1*gm2
   g1m2r  = g1m2*re
   g1m2p  = g1m2*pr
   g1m2rp = g1m2r*pr

   ig1     = 1.0d0/g1
   i2g     = 1.0d0/(2.0d0*gam)
   i2g21   = 1.0d0/(2.0d0*(gam**2-1.0d0))
   ig      = 1.0d0/gam
   im      = 1.0d0/ma
   im2     = 1.0d0/(ma*ma)
   ire     = 1.0d0/re
   igm2    = 1.0d0/gm2
   ig1m2   = 1.0d0/g1m2
   igg1m2  = 1.0d0/gg1m2
   ig1m2p  = 1.0d0/g1m2p
   ig1m2rp = 1.0d0/g1m2rp

   g1ig        =    g1*ig
   g1isqrt2g21 =    g1*sqrt(i2g21)
   n2ig        = 2.0d0*ig
   n2isqrt2g21 = 2.0d0*sqrt(i2g21)

!--- cone angle in radians and sin(theta),cos(theta)
#if (GEOMETRY == 2)||(GEOMETRY == 3)||(GEOMETRY == 4)
   theta = theta*pi/180.0d0
   sintheta = sin(theta)
   costheta = cos(theta)
#endif !GEOMETRY

!--- calculate dx, dy ...
   dx = (x_L-x_0)/(nx-1)
   dxx =dx*dx  
   dy = (y_H-y_0)/(ny-1)
   dyy = dy*dy

   t_en_1 = t_en
   inter_count = 0

#if (MULTI_TS == 1)||(MULTI_TS == 3)
   s_multits = 2
#if (TIME_INT == 1)
   r_multits = 1
#elif (TIME_INT == 2)
   r_multits = 2
#elif (TIME_INT == 3)
   r_multits = 4
#elif (TIME_INT == 4)
   r_multits = 3
#else
   stop 'ERROR.params.f90, TIME_INT not correct'
#endif   
   dt_input = dt
#endif !MULTI_TS

   end subroutine params_init

end module params
