!----------------------------------------------------------------------------------------------------------------------------------!
!                                                                                                                                  !
! Some precompiler statements.                                                                                                     !
!                                                                                                                                  !
!----------------------------------------------------------------------------------------------------------------------------------!

!----------------------------------------------------------------------------------------------------------------------------------!
! equations, geometry and dimension of the simulation                                                                              !
!----------------------------------------------------------------------------------------------------------------------------------!

! equation
! 1 Euler
! 2 Navier-Stokes
! 3 Linearized DNS
#define EQN 2

! geometry
! 1 flat plate
! 2 cone
! 3 cylindrical coordinates with generalized coordinates in x and y (orthogonal grid)
! 4 cylindrical coordinates with generalized coordinates in x and y (non-orthogonal grid)
#define GEOMETRY 1

! dimensions
! 2 2D
! 3 3D
#define SPACE_DIM 3


!----------------------------------------------------------------------------------------------------------------------------------!
! Time Integration scheme                                                                                                          !
!----------------------------------------------------------------------------------------------------------------------------------!

! Time integration
! 1 Euler Forward (1st-order)                                              
! 2 Runge-Kutta second order                                       
! 3 Runge-Kutta fourth order                                               ! recommended
! 4 Runge-Kutta third order                                                ! recommended
#define TIME_INT 3

! Multi time steping
! 0 off
! 1 on (multi time stepping consistent)
! 2 on (time step according to local grid size) use only for steady state solutions
! 3 on (time step according to local grid size) use only for steady state solutions, plus multiple evaluations at the wall
#define MULTI_TS 0

!----------------------------------------------------------------------------------------------------------------------------------!
! Spatial Discretization                                                                                                           !
!----------------------------------------------------------------------------------------------------------------------------------!

!----- order of central stencils --------------------------------------------------------------------------------------------------!

! Space discretization in x-direction
! 1 2nd-order finite differences on stretched grid
! 2 4th-order finite differences on stretched grid
! 3 6th order finite differences on stretched grid                         ! recommended for flat plate
! 4 8th order finite differences on stretched grid
! 5 finite differences user specified
#define SPACE_INT_X 3

! Space discretization in y-direction
! 1 2nd-order finite differences on stretched grid
! 2 4th-order finite differences on stretched grid                         ! recommended
! 3 6th-order finite differences on stretched grid
! 4 8th-order finite differences on stretched grid
! 5 finite differences user specified
#define SPACE_INT_Y 2

! Space discretization in z-direction
! 1 2nd order finite differences on stretched grid
! 2 4th order finite differences on stretched grid
! 3 6th order finite differences on stretched grid
! 4 4th order compact finite differences (tridiagonal system)
! 5 6th order compact finite differences (tridiagonal system)
! 6 8th order compact finite differences (tridiagonal system)
! 7 8th order compact finite differences (pentadiagonal system)
! 8 10th order compact finite differences (pentadiagonal system)
! 9 Fourier modes (only derivatives are calculated in spectral space,
!                  all oher operations are done in physical space)         ! recommended
#define SPACE_INT_Z 9

!----- order of upwind stencils ---------------------------------------------------------------------------------------------------!

! Upwinding in the x-direction
! 0 off
! 1 3rd-order
! 2 5th-order
! 3 7th-order
! 4 9th-order                                                              ! recommended
! 5 1st-order
#define UW_X 4

! Upwinding in the y-direction
! 0 off
! 1 3rd-order
! 2 5th-order
! 3 7th-order
! 4 9th-order                                                              ! recommended
! 5 1st-order
#define UW_Y 4

! Upwinding in the z-direction
! 0 off
! 1 1st order finite differences on stretched grid
! 2 3rd order finite differences on stretched grid
! 3 5th order finite differences on stretched grid
! 4 3rd order compact finite differences (tridiagonal system)
! 5 5th order compact finite differences (tridiagonal system)
! 6 7th order compact finite differences (tridiagonal system)
! 7 7th order compact finite differences (pentadiagonal system)
! 8 9th order compact finite differences (pentadiagonal system)
#define UW_Z 0

! Central stencils at the outflow (not needed)
! 0 off                                                                    ! recommended
! 1 on
#define CENTRAL_STENCILS_AT_OUTFLOW 0

! Grid stetching at the wall Boundary
! 0 off                                                                    
! 1 on									   ! recommended
#define BOUNDARY_STRETCHING 0

!----- WENO scheme ----------------------------------------------------------------------------------------------------------------!

! WENO scheme
! 0 off 
! 1 WENO5 (five point stencils which sum up to 9th-order)
#define WENO 0

! WENO in x -direction
! 0 off
! 1 WENO5 (five point stencils which sum up to 9th-order)
#define WENO_X 0

! WENO in y -direction
! 0 off
! 1 WENO5 (five point stencils which sum up to 9th-order)
#define WENO_Y 0

! WENO in z -direction
! 0 off
! 1 WENO5 (five point stencils which sum up to 9th-order)
#define WENO_Z 0 

! WENO debugging
! 0 off
! 1 on
#define WENO_DEBUG 0

! WENO scheme
! 0 off
! 3 WENO3 (three point stencils which add up to 5th-order accuracy)
! 5 WENO5 (five point stencils which add up 9th-order accuracy
#define WENO_SCHEME 3
#define WENO_IN_X 1
#define WENO_IN_Y 1

!----- TVD scheme -----------------------------------------------------------------------------------------------------------------!

! 2nd order TVD scheme
! 0 off
! 1 on
#define TVD_SCHEME 0

!----- FFT ------------------------------------------------------------------------------------------------------------------------!

! FFT as discretization in z-direction?
#undef FFT


!----------------------------------------------------------------------------------------------------------------------------------!
! Flux-splitting scheme                                                                                                            !
!----------------------------------------------------------------------------------------------------------------------------------!

! Flux-splitting
! 1 Steger-Warming
! 2 van Leer                                                               ! recommended
! 3 global Lax-Friedrich                                                   ! does not work yet (wrongly implemented)
#define FLUX_SPLIT 2

! Flux-splitting in z
! 1 Steger-Warming (not implemented)
! 2 van Leer                                                               ! recommended
! 3 Zha-Bilgen
#define FLUX_SPLIT_Z 2


!----------------------------------------------------------------------------------------------------------------------------------!
! Boundary conditions                                                                                                              !
!----------------------------------------------------------------------------------------------------------------------------------!

! Calculation of the inflow boundary condition
! 1 Supersonic (like Paul's NSCC) (recommended)                            ! recommended for flat plate
! 2 Supersonic (subsonic dpdx=0, but dens form equation of state)
! 4 for analytical test function
! 5 CBC inflow (Poinsot and Lele)                                          
! 6 Dirichlet conditions within 5 points from inflow boundary              ! recommended for cone (distrubance simulations)
! 7 CBC inflow (Poinsot and Lele) within 5 points from inflow boundary     ! recommended for cone (base flow simulations)
! 8 Symmetry (version1) 
! 9 Symmetry (version2)  
!10 Subsonic Inflow
!11 Supersonic, 2d Inflow for 3d simulations (NSCC)
#define INFLOW 6

! Calculation of wall boundary condition
! 1 no slip wall, adiabatic, dpdy = 0 , density from equation of state   
! 2 no slip wall, adiabatic, pressure extrapolated, density form eqn of state  ! recommended for base flow simulations
! 3 no slip wall, T fixed, pressure extrapolated, density form eqn of state    ! recommended for disturbance simulations
! 4 no slip wall, T fixed, dpdy = 0 , density from equation of state   
! 5 no-penetration wall for Euler solver
#define WALL 3

! Calculation of free-stream boundary
! 1 Free-stream values are calculated using Navier-Stokes equations 
!   and one-sided differences (hence, the data inside the domain).
! 2 characteritic boundary condition adopted from Paul                     ! recommended for flat plate (base flow simulations)
! 3 dphi/dy = 0 subsonic 
! 4 exponential decay condition                                            ! recommended for flat plate (distrubance simulations)
! 5 for analytical test function
! 6 CBC free-stream (= subsonic outflow, Richard)
! 7 uvel, vvel,dens, pres,temp fixed (Dirichlet)                           ! recommended for cone (base flow simulations)
! 8 Franks conditions (non-reflecting)
! 9 Dirichlet conditions within 5 points from freestream boundary
!10 Same condition as 2 but with 2nd order stencils at upper boundary        
!11 3d boundary conditions (Dirichlet)      
#define FREESTREAM 2

! Calculation of outflow boundary
! 1 Outflow values are calculated using Navier-Stokes equations 
!   and one-sided differences (hence, the data inside the domain).
! 2 Paul's old condition d2phi/dx2=0 for primitive variables               ! recommended
! 3 Paul's old condition d2phi/dx2=0 for conservative variables
! 4 subsonic outflow from Dominic, p=const, d2udx2 = 0, 
!   d2vdx2 = 0, d2Tdx2 = 0, and rho from equation of state
! 5 for analytical test function
! 6 CBC outflow (Richard)
#define OUTFLOW 2

!----- OUTFLOW RAMP ----------------------------------------------------------------------------------------------------------------!

! Outflow ramp
! 0 off
! 1 Huberts ramp
! 2 Polynomial ramp
! 3 Huberts ramp with 1st-order stencils                                   ! recommended
! 4 Polynomial ramp with 1st-order stencils
#define OUTFLOW_RAMP 0

!---- Spanwise/Azimutahl Direction -------------------------------------------------------------------------------------------------!

! full or symmetric?
! FFTSYM defined -> symmetric                                                      
! FFTSYM undefined -> full                                                 ! not yet implemented for SPACE_INT_Z == 9 (Fourier)
#define FFTSYM


!----------------------------------------------------------------------------------------------------------------------------------!
! VISCOSITY                                                                                                                        !
!----------------------------------------------------------------------------------------------------------------------------------!

! law for viscosity
! 1 Sutherlands law                                                       ! recommended
! 2 approximate viscosity law
! 3 Sutherlands law with low temperature correction
#define VISCOSITY 1


!----------------------------------------------------------------------------------------------------------------------------------!
! Filtering                                                                                                                        !
!----------------------------------------------------------------------------------------------------------------------------------!

!Apply a filter in x directions
! 0 Filter off
! 1 Filter non-compact 2nd-order 
! 2 Filter non-compact 4th-order 
! 3 Filter non-compact 6th-order 
! 4 Filter non-compact 8th-order 
! 5 Filter non-compact 10th-order 
#define FILTERX 0

!Apply a filter in y directions
! 0 Filter off                   
! 1 Filter non-compact 2nd-order 
! 2 Filter non-compact 4th-order 
! 3 Filter non-compact 6th-order 
! 4 Filter non-compact 8th-order 
! 5 Filter non-compact 10th-order 
#define FILTERY 0

!Apply a filter in z directions
! 0 Filter off
! 1 Filter non-compact 2nd-order 
! 2 Filter non-compact 4th-order 
! 3 Filter non-compact 6th-order  
! 4 Filter non-compact 8th-order 
! 5 Filter non-compact 10th-order 
! 6 Filter Fourier                                                         ! recommended
#define FILTERZ 0

!Apply filter to disturbances only
! 0 off
! 1 on
#define FILTER_DIST 0


!----------------------------------------------------------------------------------------------------------------------------------!
! Distrubance generation                                                                                                           !
!----------------------------------------------------------------------------------------------------------------------------------!

! Disturbance strip
! 0 off
! 1 continuous distrubances
! 2 pulse distrubance
#define DISTURBANCE 0

!Volume Forcing in Free stream
!0 off
!1 on
#define VOLUME_FORCE 0

! disturbance shape in x-direction
! 1 Dipole (Paul)
! 2 Monopol (Dieter)
#define DIST_X 1

! disturbance shape in z-direction
! 1 sin
! 2 parabola
! 3 Monopol (Dieter)
#define DIST_Z 1

! disturbances are ramped in  
! 0 off
! 1 on
#define DIST_RAMP 0

! Fasel Konzelmann initial disturbance (disturbance parameters are hard coded)
! 0 off
! 1 on
#define FASEL_KONZELMANN 0

!----------------------------------------------------------------------------------------------------------------------------------!
! Input                                                                                                                            !
!----------------------------------------------------------------------------------------------------------------------------------!

!read initial condition
! 1 from Profcom
! 2 from continuation file
#define READ_IC 1

! read-in dimensions
! 2 read in 2D field
! 3 read in 3D field
#define READ_DIM 2

! read-in physical space or spectral space
! 1 read in physical space
! 2 read in spectral space
#define READ_PHYS_SPEC 1

! read-in different file for bc, buffer, etc
! 0 off
! 1 on
#define READ_BF 0

!grid in x -driection
! 1 generated in the code
! 2 read in from grid file
#define GRIDX_IN 2

!grid in y-direction
! 1 generated in the code
! 2 read in from grid file
#define GRIDY_IN 2

!grid in z-direction
! 1 generated in the code
! 2 read in from grid file
#define GRIDZ_IN 1

!grid for generalized cylindrical coordinates
! 1 generated in the code
! 2 read in from grid file
#define GRIDGC_IN 2

!Data input
! 1 one processor input (reading in only one z-slice at a time)            ! recommended for large simulations on marin
! 2 mpi-io
#define MPI_IN 1

!time dependent inflow with FT data
! 0 no
! 1 yes, using N-factors to rescale amplitudes for each individual mode
! 2 yes, using the same amplitude as in the TDI input file
! 3 yes, using scaling factors to scale the amplitude for each individual mode 
#define TDI_FT 0


!----------------------------------------------------------------------------------------------------------------------------------!
! output                                                                                                                           !
!----------------------------------------------------------------------------------------------------------------------------------!

!final output 
! 1 write out in physical space
! 2 write out in spectral space
#define PHYS_SPEC 1

!intermediate output 
! 0 write out total flow variables (physical space)
! 1 write out difference between instantaneous flow variable and initial condition
! 3 write out total flow vairables (spectral space)
#define INTER_DIFF 0

!define which variables you want to write out
! 0 do not write out
! 1 write out
#define INTER_UVEL 1
#define INTER_VVEL 0
#define INTER_TEMP 1
#define INTER_DENS 1
#define INTER_PRES 1
#define INTER_WVEL 0
#define INTER_DUVEL 0
#define INTER_DVVEL 0
#define INTER_TOEN 0
#define INTER_DWVEL 0

!Data and grid output
! 1 one processor output
! 2 mpi-io
! 3 one processor output (writing out only one z-slice at a time)          ! recommended for large simulations on marin
#define MPI_OUT 3

!----------------------------------------------------------------------------------------------------------------------------------!
! Parallelization                                                                                                                  !
!----------------------------------------------------------------------------------------------------------------------------------!

! MPI parallelization
! 0 off
! 1 on                                                                     ! recommended
#define MPI_PAR 1

!domain decomposition
! 0 determined by code using subroutine from Andi Gross                    ! recommended
! 1 determined by user change in dat.in
#define DOMAINS 0

!----------------------------------------------------------------------------------------------------------------------------------!
! Miscellaneous                                                                                                                    !
!----------------------------------------------------------------------------------------------------------------------------------!

! DEBUG: if defined -> debug arrays are allocated.
#undef DEBUG 

!set record length
! 1 for HP-alpha(keg), SGI-Altix(marin), Itanium(echigo), linux
! 4 for SGI(sol), Cray(sapphire,jade)
#define REC_LENGTH 1

!allocate arrays dynamic/static
! 1 dynamic                                                                ! recommended
! 2 static (must specify local_nx, local_ny in params.f90)                 ! does not work at the moment
! 3 static new (must specify local_nx, local_ny in params.f90)             ! implementation in progress
#define ALLO 1

! timing of selected subroutines
! 0 off                                                                    ! recommended
! 1 on
#define TIMEALL 0

! Byte swapping (little-big endian) 
! 0 off                                                                    ! recommended for sol
! 1 on                                                                     ! recommended for marin,keg,echigo,sapphire
#define BYTESWAP 0

!different discretization routines
!0 calculating derivatives in routine with diffusive terms                 ! recommended, because faster
!1 calculating derivatives first and then diffusive terms second 
#define  DISCRET 0

!implementations using different memory allocations
!0 more memory (recommended if enough memory is available)
!1 less memory (recommended if memory limitations do exist, use only if DISCRET == 0)
#define  MEMORY 0

!detects shock and changes stencils around shock
!0 off                 
!1 on (changes stencil at specfied "is" value)
!2 on (chenges stencil 10 pts before specified "is" value)
#define SHOCK 0

!Forcing of the initial condition
!0 off                 
!1 on
!2 override zero mode with base flow after each full Runge_Kutta step (only with Fourier in z-direction)
#define FORCE_INITIAL 0

!Forcing a parallel flow
!0 off                 
!1 for continuation file and base flow file
!2 only for base flow file 
#define ENFORCE_PARALLEL_FLOW 0

!Filter more modes at the beginning of the domain. This is usefull if due the small physical width close to the cone tip
!   the grid line spacing in z-direction is to small and therefore numerically unstable.
!0 off
!1 on
#define FILTMODES 0

!For writing out the coordinate transformation terms for optione GEOMETRY == 3
!0 off
!1 on
#define OUTPUT_GC_TERMS 0

!handeling of selected terms using Fourier transforms
!0 as before
!1 new
#define FT_TERMS 0

!handeling of selected terms using derivatives in z
!0 as before
!1 new
#define DZ_TERMS 0

!----------------------------------------------------------------------------------------------------------------------------------!
! Moving Domain                                                                                                                !
!----------------------------------------------------------------------------------------------------------------------------------!

!Track boundaries of transient data
!0 off
!1 on
#define TRANS_REALLOC 0

!Which boundaries shold be tracked
!inflow
!0 off
!1 for disturbances
!2 for converging base flow
#define TRANS_IN 0
!outflow
!0 off
!1 for disturbances
!2 for converging base flow (not fully functional)
#define TRANS_OUT 0
!freestream
!0 off
!1 for disturbances
#define TRANS_FREE 0
!spanwise/azimuthal (only applicable if 3D and SPACE_INT_Z .NE. 9)
!0 off
!1 for disturbances
#define TRANS_SPANAZI 0

!Ramp at moving domain boundaries
!0 off
!1 on
#define TRANS_RAMP 0

!adjustment of epsilon
!0 off
!1 on
#define EPSILON_ADJUSTMENT 0

!----------------------------------------------------------------------------------------------------------------------------------!
! Averaging                                                                                                                !
!----------------------------------------------------------------------------------------------------------------------------------!

!Average Skin friction
!0 off
!1 on
#define AVERAGE_SKIN_FRICTION 0 


!----------------------------------------------------------------------------------------------------------------------------------!
! Roughness Element                                                                                                                !
!----------------------------------------------------------------------------------------------------------------------------------!

!Roughness Element
!off 0
!2D  1
!3D  2
#define ROUGH 0

!Roughness Element Temperature
!adiabat     1
!isothermal  2
#define  ROUGH_TEMP 1

!Roughness Element Pressure
!from wall normal momentum equation     1
!dpdn = 0                               2
#define  ROUGH_PRES 2

!----------------------------------------------------------------------------------------------------------------------------------!
! ROUGHNESS/CAVITIES (chader)                                                                                                                !
!----------------------------------------------------------------------------------------------------------------------------------!
! Roughness Element
! off 0
! on 1
#define ROUGHNESS 0

!!! ROUGHNESS and POROSITY can't be 1 at the same time !!!
! Porous Coating 
! off 0
! on 1
#define POROSITY 0

! Roughness Type
! isolated 0
! distributed 1
#define ROUGHNESS_TYPE 1

!----------------------------------------------------------------------------------------------------------------------------------!
! Error check                                                                                                                      !
!----------------------------------------------------------------------------------------------------------------------------------!
! SPACE_DIM
#if (SPACE_DIM == 2)
#undef FFT
#endif !SPACE_DIM

! FFT
#if defined(FFT)
#define SPACE_DIM 3
#define UW_Z 0
#define OUTFLOW 2
#define FREESTREAM 2
#endif !FFT
